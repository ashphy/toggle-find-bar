/**
 * Toggle Find Bar
 * @author ashphy (ne.vivam.si.abis@gmail.com)
 * @license The MIT License
 */

var toggleFindBar = {
	show: function(){
		var findToolBar = document.getElementById("FindToolbar");
		if (findToolBar == null || findToolBar.hidden) {
			//Show find tool bar
			gFindBar.onFindCommand();
		} else {
			//Close find tool bar
			findToolBar.close();
		}
	}
};

//Replace default keyconfig of showing find tool bar with this.
window.addEventListener('load', function() {
	var key = document.getElementById('key_find');
	key.parentNode.appendChild(key);
}, false);